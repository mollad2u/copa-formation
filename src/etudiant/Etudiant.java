package etudiant;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Classe Etudiant
 * @author Guillaume
 *
 */
public class Etudiant {

	/** Liste des notes */
	private HashMap<String, ArrayList<Float>> lNotes;
	
	/** Identite */
	private Identite identite;
	
	/** Formation */
	private Formation formation;
	
	/** Construit un etudiant */
	public Etudiant(Identite id, Formation form){
		this.identite=id;
		this.formation=form;
		this.lNotes = new HashMap<String, ArrayList<Float>>();
		
	}
	
	
	/** Methode getIdentite 
	 * Retourne l'identite
	 */
	public Identite getIdentite(){
		return this.identite;
	}
	
	
	/** Methode getFormation
	 * Retourne la formation
	 */
	public Formation getFormation(){
		return this.formation;
	}
	
	/** Methode getlNotes 
	 * Retourne la liste de note
	 */
	public HashMap<String,ArrayList<Float>> getlNotes(){
		return this.lNotes;
		
	}


	
	public void ajouterNote(String mat, Float note){
			if(this.lNotes.containsKey(mat)){
				if (note >= 0 || note <= 20){
										ArrayList<Float> tmp = lNotes.get(mat);
					tmp.add(note);
					this.lNotes.put(mat, tmp);
				}
		}
	}
	
	public void ajouterListeNotes(String matiere,ArrayList<float> notes){
		if (lNotes.containsKey(matiere))
			this.lNotes.put(matiere, lnotes);
		
	}
	
	public float moyenneMatiere(String matiere){
		float moy=-1;
		if (this.lNotes.containsKey(matiere)){
			float somme=0;
			for (int i=0;i<this.lNotes.get(matiere).size();i++){
				somme+=this.lNotes.get(matiere).get(i);
			}
			moy=somme/this.lNotes.get(matiere).size();
					
			
		}
		return moy;
		
	}
	
	public float moyenneGeneral(){
		float moy=-1;
	float somme=0;
	Iterator<String> it=lNotes.keySet().iterator();
	while (it.hasNext()){
		somme+=this.moyenneMatiere(it.toString());
		System.out.println(it.toString());
		it.next();
	
		
		
	}
	moy=somme/lNotes.size();
	return moy;
			
		
	}
	
	
	
	
	
	/** Methode toString */
	public String toString() {
		return "Etudiant [lNotes=" + lNotes + ", identite=" + identite + ", formation=" + formation + "]";
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
