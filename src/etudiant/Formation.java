package etudiant;

import java.util.HashMap;

public class Formation {
	
	private String id;
	private HashMap<String,Integer> lMatiere;
	
	public Formation(String formation,HashMap<String,Integer> lmat){
		this.id=formation;
		if (lmat!=null){
		this.lMatiere=lmat;
		}
		
		
		
	}

	public String getId() {
		return id;
	}

	public HashMap<String, Integer> getlMatiere() {
		return lMatiere;
	}

	@Override
	public String toString() {
		return "Formation [id=" + id + ", liste des matiere =" + lMatiere + "]";
	}
	
	public int coefMatiere(String matiere){
		if (this.lMatiere.containsKey(matiere))
			return lMatiere.get(matiere);
		else
			return -1;
				}
	
	public void ajouterMatiere(String matiere,int coef){
		if (!lMatiere.containsKey(matiere))
			lMatiere.put(matiere, coef);
		
	}
	
	public void supprimerMatiere(String matiere){
		if (lMatiere.containsKey(matiere))
			lMatiere.remove(matiere);
	}


	


}
