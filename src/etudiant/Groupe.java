package etudiant;

import java.util.ArrayList;
import java.util.Iterator;

public class Groupe {

	private ArrayList<Etudiant> lEtudiant;
	private Formation formation;

	public Groupe(Formation f, ArrayList<Etudiant> etudiant) {
		if (f != null)
			this.formation = f;
		if (etudiant != null)
			this.lEtudiant = etudiant;

	}

	public ArrayList<Etudiant> getlEtudiant() {
		return lEtudiant;
	}

	public Formation getFormation() {
		return formation;
	}

	public void ajouterEtudiant(Etudiant etudiant) {
		if (!this.lEtudiant.contains(etudiant))
			this.lEtudiant.add(etudiant);

	}

	public void supprimerEtudiant(Etudiant etudiant) {
		if (this.lEtudiant.contains(etudiant))
			this.lEtudiant.remove(etudiant);

	}

	public float moyenneMatiere(String matiere) {
		float moy = -1;
		float somme = 0;

		if (this.formation.getlMatiere().containsKey(matiere)) {
			for (Etudiant e : this.lEtudiant) {
				somme += e.moyenneMatiere(matiere) * this.formation.coefMatiere(matiere);

			}
			moy = somme / this.formation.getlMatiere().size();
		}
		return moy;

	}
	
	public float moyenneGeneral(){
		float moy=-1;
	float somme=0;
	for (Etudiant e:this.lEtudiant){
		somme+=e.moyenneGeneral();
		
	}
	moy=somme/this.lEtudiant.size()
	
	return moy;
			
		
	}

}
