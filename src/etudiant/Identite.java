package etudiant;

/**
 * Classe Identite
 * @author Guillaume
 *
 */
public class Identite {
	
	/** Numero d'identification person*/
	private String nip;
	
	/** Nom */
	private String nom;
	
	/** Prenom */
	private String prenom;
	
	
	/**
	 * Constructeur Identite
	 * Construit une identite
	 * @param nip2
	 * @param nom2
	 * @param prenom2
	 */
	public Identite(String nip2, String nom2, String prenom2){
		this.nip=nip2;
		this.nom=nom2;
		this.prenom=prenom2;
		
	}
	
	/** Retourne le NIP */
	public String getNip(){
		return this.nip;
	} 
	 
	/** Retourne le nom */
	public String getNom(){
		return this.nom;
	}
	
	/** Retourne le prenom */
	public String getPrenom(){
		return this.prenom;
	}

	
	/** Retourne l'affichage de l'identite */
	public String toString() {
		return "Identite [Nip=" + nip + ", Nom=" + nom + ", Prenom=" + prenom + "]";
	}

	/** Methode hashCode */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nip == null) ? 0 : nip.hashCode());
		result = prime * result + ((nom == null) ? 0 : nom.hashCode());
		result = prime * result + ((prenom == null) ? 0 : prenom.hashCode());
		return result;
	}

	/** Methode equals */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Identite other = (Identite) obj;
		if (nip == null) {
			if (other.nip != null)
				return false;
		} else if (!nip.equals(other.nip))
			return false;
		if (nom == null) {
			if (other.nom != null)
				return false;
		} else if (!nom.equals(other.nom))
			return false;
		if (prenom == null) {
			if (other.prenom != null)
				return false;
		} else if (!prenom.equals(other.prenom))
			return false;
		return true;
	}

	
	
	
	
}
