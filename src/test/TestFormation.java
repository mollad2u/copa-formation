package test;

import java.util.HashMap;

import org.junit.After;
import org.junit.Test;

import etudiant.Formation;
import junit.framework.Assert;

public class TestFormation {

	HashMap<String, Integer> lmat;
	
	
	@Test
	public void testInit() {
		lmat = new HashMap<String,Integer>();
		Formation forma = new Formation("F1", lmat);

		lmat.put("Maths", 3);
		lmat.put("Francais", 2);
		lmat.put("SVT", 1);
		lmat.put("Sport", 1);
		
	}
	
	
	@After
	public void test_coefficient(){
		Formation forma = new Formation ("F1", lmat);
		
		int coeff = forma.coefMatiere(("Maths"));
		Assert("Le coefficent est exact",3,coeff);
		
	}


}
